---
date: 2021-12-07T16:50:16+02:00
title: GCP Professional Cloud Architect
pre: "<b>1.1. </b>"
weight: 110
---

## Professional Cloud Architect

公式GoogleCloud認定
教本日本語訳 +α

### Chapter1

試験のイントロダクション

### Chapter2

ビジネス要件デザイン

### Chapter3

技術要件デザイン

### Chapter4

コンピュートシステムデザイン

### Chapter5

ストレージシステムデザイン

### Chapter6

ネットワークデザイン

### Chapter7

セキュリティと法的コンプライアンスへのデザイン

### Chapter8

信頼性デザイン

### Chapter9

技術プロセスの分析と定義

### Chapter10

ビジネスプロセスの分析と定義

### Chapter11

開発と運用

### Chapter12

統合計画

### Appendix

Answers to Review Questions

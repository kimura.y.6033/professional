---
date: 2021-12-07T16:50:16+02:00
title: Appendix 
pre: "<b>1.1.13 </b>"
weight: 123
---

## References
- Security
  - [IAM](https://cloud.google.com/iam/docs/?hl=ja)
- Compute
  - [Compute Engine](https://cloud.google.com/compute?hl=ja)
  - [App Engine](https://cloud.google.com/appengine?hl=ja)
  - [Cloud Functions](https://cloud.google.com/functions?hl=ja)
- Storage
  - [Cloud Storage](https://cloud.google.com/storage/?hl=ja&utm_source=google&utm_medium=cpc&utm_campaign=japac-JP-all-ja-dr-bkws-all-super-trial-e-dr-1009882&utm_content=text-ad-none-none-DEV_c-CRE_540879852690-ADGP_Hybrid%20%7C%20BKWS%20-%20PHR%20%7C%20Txt%20~Storage%20~%20Cloud%20Storage_google%20storage-en-KWID_43700065767096008-aud-970366092687%3Akwd-636935148&userloc_1009282-network_g&utm_term=KW_google%20storage&gclid=Cj0KCQiA-eeMBhCpARIsAAZfxZCaSegTBrSjPp2CokGNv55xrapfDOumKQ18NT_mWbgZIXVW0yknrqYaAkeXEALw_wcB&gclsrc=aw.ds)
  - [Firestore](https://cloud.google.com/firestore?hl=ja)
- データ分析
  - [Dataflow](https://cloud.google.com/dataflow?hl=ja)
  - [Pub/Sub](https://cloud.google.com/pubsub?hl=ja)
  - [Dataprep※出ない？](https://cloud.google.com/dataprep?hl=ja)
  - [BigQuery](https://cloud.google.com/bigquery?hl=ja)
  - [Data Catalog](https://cloud.google.com/data-catalog?hl=ja)
- RDB
  - [Cloud Spanner](https://cloud.google.com/spanner?hl=ja)
  - [Cloud SQL](https://cloud.google.com/sql?hl=ja)
- NoSQL
  - [Firebase Realtime Database※出ない？](https://firebase.google.com/docs/database?hl=ja)
  - [Bigtable](https://cloud.google.com/bigtable?hl=ja)
- 運用
  - [Operations(旧Stackdriver)](https://cloud.google.com/products/operations?hl=ja)
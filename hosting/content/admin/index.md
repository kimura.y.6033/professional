---
title: 管理者ガイド
date: 2020-06-16
pre: "<b>9. </b>"
weight: 999
---

## 登録方法

[Firestoreの画面](https://console.firebase.google.com/project/gcp-study-e51e6/firestore/data/~2Fusers~2Fgpca?hl=ja)からメールアドレスを登録する。

コレクション「allows」にgmailアドレスでドキュメントを登録し、フィールド「paths」に許可するパスの文字列リストを登録する。
例) `/admin`と`/gcp`から始まるパスをすべて許可する
- /admin
- /gcp
- /azure
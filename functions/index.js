const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();
const db = admin.firestore();

const path = require("path");
const express = require("express");
const cookieParser = require("cookie-parser");
const app = express();
app.use(cookieParser());

// unixtime で最終デプロイ時刻を固定値で取得
const api_deployed_unixtime = Math.floor(new Date().getTime() / 1000);
function pretty(obj) {
  return JSON.stringify(obj, null, " ");
}
function log(level, req_id, message) {
  console[level](
    `API-VERSION: ${api_deployed_unixtime} / REQ-ID: ${req_id} / ${message}`
  );
}
app.get("*", (req, res, next) => {
  const req_id = Math.floor(new Date().getTime());
  log("debug", req_id, `req.path: ${req.path}`);
  if (
    req.path === "" ||
    req.path === "/" ||
    req.path === "/403/" ||
    req.path === "/500/" ||
    req.path.startsWith("/js") ||
    req.path.startsWith("/css") ||
    req.path.startsWith("/img") ||
    req.path.startsWith("/fonts") ||
    req.path.startsWith("/webfonts") ||
    req.path.startsWith("/mermaid.js") ||
    req.path.startsWith("/index.json") ||
    req.path.startsWith("/favicon.png") ||
    req.path.endsWith(".ico")
  ) {
    log("debug", req_id, `認証の対象外です。 / req.path: ${req.path}`);
    return next();
  }
  if (!(req.cookies && req.cookies.__session)) {
    // log("debug", req_id, `token cookieがありません。`);
    // log("debug", req_id, `rawHeaders: ${pretty(req.rawHeaders)}`);
    // log("debug", req_id, `cookies: ${JSON.stringify(req.cookies, null, " ")}`);
    res.status(403).redirect("/403/");
    return null;
  }
  return admin
    .auth()
    .verifyIdToken(req.cookies.__session, true)
    .catch((error) => {
      return Promise.reject(
        new Error(
          `idTokenのverifyに失敗しました。: ${JSON.stringify(error, null, " ")}`
        )
      );
    })
    .then((decodedToken) => {
      log("debug", req_id, `decodedToken: ${JSON.stringify(decodedToken)}`);
      return db.collection("allows").doc(decodedToken.email).get();
    })
    .catch((error) => {
      log(
        "error",
        req_id,
        `Firestoreへのアクセスでエラーが発生しました。: ${error}`
      );
      res.status(500).redirect("/500/");
      return null;
    })
    .then((doc) => {
      if (!doc.exists) {
        return Promise.reject(
          new Error(`登録されていないemailです。: ${decodedToken.email}`)
        );
      } else if (
        doc.data().paths.filter((path) => req.path.startsWith(path)).length ===
        0
      ) {
        log(
          "debug",
          req_id,
          `許可されていないパスです。/ email: ${decodedToken.email} / path: ${req.path}`
        );
        return Promise.reject(
          new Error(
            `許可されていないパスです。/ email: ${decodedToken.email} / path: ${req.path}`
          )
        );
      } else {
        log("info", req_id, "認証処理が正常終了しました。");
        return next();
      }
    })
    .catch((error) => {
      log("warn", req_id, error.message);
      res.status(403).redirect("/403/");
      return null;
    });
});
app.use(express.static(path.join(__dirname, "/public/")));
// us-central1でないとHostingからRewriteできない
// exports.hosting = functions.region("us-central1").https.onRequest(app);
exports.hosting = functions.https.onRequest(app);
